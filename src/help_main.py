#!/usr/bin/python
# coding:utf-8
# Help-me-carry,mainノード

import rospy
from std_msgs.msg import String, Bool

import time

import os  # espeakで使う

import difflib  # 類似度を計算するライブラリ

# それぞれのフラグを'False'に設定
followme_flag = 'False'
speak_flag = 'False'
stop_flag = 'False'
take_flag = 'False'

# stringの文字を送る関数


def string_send(str, pub):
    send = String()
    send.data = str
    pub.publish(send)

# 類似度を計算する関数


def get_similar(listX, listY):
    s = difflib.SequenceMatcher(None, listX, listY).ratio()
    return s

# Juliusから受け取った文字の処理


def callback(data):
    # グローバル変数のflagの使用のため
    global followme_flag
    global speak_flag  # 発話が終了したかどうか
    global stop_flag
    global take_flag

    request = data.data  # 受け取った文字を'request'とおく

    # 類似度計算
    request_list = ['followme', 'stopfollowme', 'takebag', 'Yes', 'No']

    max = 0
    answer = ''

    for q in request_list:
        level = get_similar(request, q)
        if level > max:
            max = level
            answer = q

    # ---ここから条件分岐の処理
    # ---Follow me
    if followme_flag != 'Finish':
        if answer == request_list[0]:  # 'follow me'と言われた時
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            followme_flag = 'True'  # followme_flagを'True'にする
            wait("speak_flag")  # recognitionがかえってくるまで待つ

            speak = 'Do you want me to start follow me?'  # 「follow me で正しいですか？」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る

            wait("speak_flag")  # 発話が終わった合図が来るまで待つ
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

        # follow meが'Yes'の時の処理
        if (answer == request_list[3]) and (followme_flag == 'True'):
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            followme_flag = 'Finish'  # followme_flagを'True'にする
            wait("speak_flag")  # recognitionがかえってくるまで待つ

            # 「到着した時に'stop follow me'と言ってください」
            speak = 'When we arrive at the destination, please say stop follow me.'
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る

            wait("speak_flag")  # 発話が終わった合図が来るまで待つ

            # time.sleep(2) # 顔を覚えている（?）間の時間

            string_send('start', pub)  # 制御に'start'をpublish (follow me 開始)

            speak2 = 'I am ready to start follow me.'  # 「準備ができた」ことを伝える
            print speak2
            string_send(speak2, pub_speak)  # 発話の文字列を送る

            wait("speak_flag")
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

        if (answer == request_list[4]) and (followme_flag == 'True'):
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            followme_flag = 'False'
            wait("speak_flag")

            speak = 'OK. Please say the command again.'  # 「もう一度お願いします」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る
            wait("speak_flag")
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

    # ---Stop follow me
    if (followme_flag == 'Finish') and (stop_flag != 'Finish'):
        if answer == request_list[1]:  # 'Stop follow me'と言われた時
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            stop_flag = 'True'
            wait("speak_flag")

            speak = 'Do you want me to stop follow me?'  # 「follow me で正しいですか？」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る
            wait("speak_flag")
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

        # Stop follow meが'Yes'の時の処理
        if (answer == request_list[3]) and (stop_flag == 'True'):
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            stop_flag = 'Finish'
            wait("speak_flag")

            string_send('stop', pub)  # 制御に'stop follow me'をpublish
            speak = 'OK, I will stop. Please give me next command.'  # 「ストップします」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る
            wait("speak_flag")
            string_send('stop', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

        # Stop follow meが'No'の時の処理
        if (answer == request_list[4]) and (stop_flag == 'stopfollowme'):
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            stop_flag = 'False'
            wait("speak_flag")

            speak = 'OK. Please say the command again.'  # 「もう一度お願いします」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る
            wait("speak_flag")  # speakerが返ってくるまでまつ
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

    # --- Take this bag to ~
    if (followme_flag == 'Finish') and (stop_flag == 'Finish') and (take_flag != 'Finish'):
        if answer == request_list[2]:  # かばんを置く場所を言われた時
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            take_flag = 'True'
            wait("speak_flag")  # 'recognition'が返ってくるまで待つ

            speak = 'Do you want me to take this bag?' # 「かばんを持って行くのですか」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る

            wait("speak_flag")  # speakerが返ってくるまで待つ
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

        # かばんを置く場所が'Yes'の時の処理
        if (answer == request_list[3]) and (take_flag == 'True'):
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            take_flag = 'Finish'
            wait("speak_flag")  # 'recognition'が返ってくるまで待つ

            # request = request.rsplit(',') # もし行き先の情報が必要ならコメントアウトを外してください
            #place = request[len(request) - 1]
            # string_send(place,pub) # 制御に'stop follow me'をpublish

            speak = 'Please put the bag on the turtle bot.'  # 「タートルボットの上にかばんを置いてください」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る
            wait("speak_flag")  # 'speaker'が返ってくるまで待つ
            # string_send('start',pub_ju_start) # 'start'という文字列を送って、Juliusの認識再開

        # かばんを置く場所が'No'の時の処理
        if (answer == request_list[4]) and (take_flag == 'True'):
            string_send('stop', pub_ju_stop)  # 'stop'という文字列を送って、julius止める
            take_flag = 'False'
            wait("speak_flag")  # 'recognition'が返ってくるまで待つ
            speak = 'OK. Please say the command again.'  # 「もう一度お願いします」
            print speak
            string_send(speak, pub_speak)  # 発話の文字列を送る
            wait("speak_flag")  # 'speaker'が返ってくるまで待つ
            string_send('start', pub_ju_start)  # 'start'という文字列を送って、Juliusの認識再開

# Juliusの認識結果を受け取る関数


def listener():
    rospy.Subscriber('result', String, callback)
    rospy.init_node('help_listener', anonymous=True)
    rospy.spin()

# 発話終了のメッセージを受け取る関数


def speak_signal(message):
    global speak_flag
    speak_flag = message.data
    print speak_flag

# speak_flagが'recognition'か'speaker'になるまでループし続ける関数


def wait(target):
    if target == "speak_flag":
        global speak_flag
        speak_flag = ''
    while (speak_flag != 'recognition') and (speak_flag != 'speaker'):
        pass


if __name__ == '__main__':
    # 制御班にFollowme, Stop, かばんを置く場所のメッセージ送信
    pub = rospy.Publisher('follow_me', String, queue_size=10)
    pub_speak = rospy.Publisher(
        'speaker', String, queue_size=10)  # 発話のノードに文字列を送る
    pub_ju_start = rospy.Publisher(
        'recognition_start', String, queue_size=10)  # juliusの操作側に指示を送る
    pub_ju_stop = rospy.Publisher(
        'recognition_stop', String, queue_size=10)  # juliusの操作側に指示を送る
    rospy.Subscriber('finish', String, speak_signal)  # 発話が終わった指示を受けとる

    listener()
